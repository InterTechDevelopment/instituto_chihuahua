@extends('layouts.plantilla')

@section('content')
<div class="container">
  <!-- Content Row -->
  <div class="row">
    <div class="col-md-4 mb-5">
      <div class="card h-100">
        <div class="card-body">
          <h2 class="card-title">Información Alumno</h2>
          <p class="card-text">
            Nombre: <b> {{ Auth::user()->name }}</b> <br>
            Dirección: <b> {{ Auth::user()->direction }}</b> <br>
            Nivel de Estudios: <b> {{ Auth::user()->studio }}</b> <br>
            Escuela proveniente: <b> {{ Auth::user()->name_school }}</b> <br>
            Correo: <b> {{ Auth::user()->email }}</b> <br>
          </p>
        </div>
        <div class="card-footer">
          <a href="#" class="btn btn-primary btn-sm">More Info</a>
        </div>
      </div>
    </div>
    <!-- /.col-md-4 -->
    <div class="col-md-4 mb-5">
      <div class="card h-100">
        <div class="card-body">
          <h2 class="card-title">Información Escolar</h2>
          <p class="card-text">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod tenetur ex natus at dolorem enim! Nesciunt pariatur voluptatem sunt quam eaque, vel, non in id dolore voluptates quos eligendi labore.
          </p>
        </div>
        <div class="card-footer">
          <a href="#" class="btn btn-primary btn-sm">More Info</a>
        </div>
      </div>
    </div>

    <!-- /.col-md-4 -->
    <div class="col-md-4 mb-5">
      <div class="card h-100">
        <div class="card-body">
          <h2 class="card-title">Pagos</h2>
          <p class="card-text">Se le recuerda que el pago total es de: $789290. </p>
        </div>
        <div class="card-footer">
          <a href="{{url('pago')}}" class="btn btn-primary btn-sm">Realizar pago</a>
        </div>
      </div>
    </div>
    <!-- /.col-md-4 -->
  
  </div>
  <!-- /.row -->
  <div class="row bg-faded mt-2">
    <div class="col-4 mx-auto d-flex justify-content-center flex-wrap">
        <img src="{{ asset('image/home-1.jpg') }}"/>
    </div>
  </div>
</div>
@endsection
