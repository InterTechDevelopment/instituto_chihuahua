@extends('layouts.plantilla')
@section('content')
<div class="container">
    <!-- Content Inicio -->
    <div class="align-items-center my-4">
        <h1 class="s_title_thin" style="text-align: center; font-size: 62px;">
            Adminisiones
        </h1>
        <p class="lead text-justify">
            Intensamente curiosos e impulsados ​​a explorar, las personas de CIT valoran el 
            pensamiento analítico riguroso, el ingenio, la resolución práctica de problemas 
            y las grandes ideas nuevas (se abre en una nueva ventana). Estimulante, solidaria 
            y juguetona, la comunidad CIT (se abre en una ventana nueva) se convierte, para 
            muchos estudiantes, en un segundo hogar.
        </p>
        <p class="lead text-justify">
            En nuestras admisiones de pregrado, posgrado y profesionales, buscamos solicitantes 
            cuyas fortalezas, intereses y valores coincidan con CIT. Debido a que el Instituto 
            se basa en la idea de que el talento y las buenas ideas pueden venir de cualquier 
            parte, somos una comunidad notablemente diversa (se abre en una nueva ventana), que 
            atrae a estudiantes de los 32 estados y de 118 países. Muchos son miembros de la 
            primera generación en su familia que tienen la oportunidad de educación superior. Por 
            encima de todo, nuestros estudiantes aprenden a no tener miedo a los problemas difíciles, 
            haciendo de su educación CIT un trampolín para muchas carreras (se abre en una nueva 
            ventana) y una preparación práctica para la vida.
        </p>
        <div class="row bg-faded mt-2">
            <div class="col-4 mx-auto d-flex justify-content-center flex-wrap">
                <img src="{{ asset('image/admisiones-1.jpg') }}"/>
            </div>
        </div>
    </div>
</div>
@endsection